package main.groovy

import java.util.regex.Pattern

class Parser {
    /** Matches any text surrounded by ${ } as well as an escaped version \${} **/
    private static final Pattern REGEX_PATTERN = Pattern.compile($/(?:\\)?\$\{(.+?)\}{1,}/$)

    def map = [:]

    /**
     * Adds a key/value mapping to the parser. When parsing strings, a ${key} will be replaced
     * by its mapping, if present, in map[key]
     *
     * @param key the key that the specified value should    be replaced with when parsing a string
     * @param value the value to replace the key with when encountered during a parse
     */
    def add(String key, String value) {
        map[key] = value
    }

    /**
     * Remove a key from the parser's map
     * @param key
     */
    def remove(String key) {
        map.remove(key)
    }

    /**
     * Parses the passed string, replacing occurrences of ${key} with the key's mapping. If no mapping is found,
     * the key will be replaced with an error message.
     *
     * Keys can have a number of optional enhancements:
     *  ${key:DefaultValue} -- if no mapping is found for this key, the value of DefaultValue is returned
     *  ${?key}             -- optional, no error will be inserted into the parsed string
     *  ${key;0,2}          -- returns a substring of the resulting mapping using the passed values as
     *                         the start and end index
     *
     * @param string the string that will be parsed
     * @return a fully parsed string with all keys resolved to their mappings according to the rules state above
     */
    String parse(String string) {
        return string.replaceAll(REGEX_PATTERN, processKey)
    }

    def processKey = { match ->
        if (escaped(match)) { // if this match is escaped...
            return match[0].substring(1) // return the unresolved variable, minus the escape character
        }

        /** fullKey is the contents of the ${} wrapper, including options **/
        String fullKey = match[1]

        if(fullKey.contains('${')) {
            fullKey = parse(fullKey + '}')
        }

        int defaultDelimiterIndex = fullKey.indexOf(':')
        int formatDelimiterIndex = fullKey.indexOf(';')

        /** key will be our actual key to the map without any optional parameters **/
        String key = (defaultDelimiterIndex < 0) ? fullKey : fullKey.substring(0, defaultDelimiterIndex)

        /** replaced will be the resolved value for this key that we will return once processing is finished **/
        String replaced

        if (formatDelimiterIndex > -1) { // if we have substring formatting...
            def args = key.substring(formatDelimiterIndex + 1)?.split(',') // grab the args for the substring

            // get the key for the map
            key = key.substring(0, formatDelimiterIndex)

            // get the mapped value, and substring it using the args
            replaced = map[key]?.substring(args[0].toInteger(), args[1].toInteger())
        } else {
            replaced = map[key] // key should be good, grab the value from the map
        }

        if (!replaced) { // if no mapping was found for the key...
            if (defaultDelimiterIndex > 0) {
                return fullKey.substring(defaultDelimiterIndex + 1) // if we have a default value, return that
            }

            if (key.indexOf('?') == 0) { // if the value is optional
                replaced = map[key.substring(1)] // strip off the ? and see if the key is registered
                return replaced ?: "" // return the value if so, otherwise return an empty string since it is optional
            }

            return "[Error: no mapping found for '$key']" // no mapping was found for this variable, return an error message
        }

        if (replaced.matches(REGEX_PATTERN)) { // if we still match the pattern, call recursively to resolve nested variables
            replaced = parse(replaced)
        }

        return replaced
    }

    private Boolean escaped(match) {
        return match[0].charAt(0) == '\\'
    }
}
