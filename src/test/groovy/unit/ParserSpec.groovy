package test.groovy.unit

import main.groovy.Parser
import spock.lang.Specification

/**
 * Note: test cases are simplistic based on the examples provided in the directions.
 * I did not test combining features. If that is a requirement, make me a failing
 * test case and I'll get it green! :)
 *
 * Also not tested (in this release!) is elegantly handling bad user input.
 */
class ParserSpec extends Specification {
    def "test parser for example data"() {
        when:
        Parser parser = new Parser()
        parser.add("foo", '${bar}')
        parser.add("bar", "Hello")
        parser.add("baz", "World")
        def string = '${foo} ${baz}!'

        then:
        parser.parse(string) == "Hello World!"
    }
    
    def "test a bunch of nesting"() {
        when:
        Parser parser = new Parser()
        parser.add("foo", '${bar}')
        parser.add("bar", '${baz}')
        parser.add("baz", '${buzz}')
        parser.add("buzz", '${fizz}')
        parser.add("fizz", "Woah")
        def string = '${foo}!'

        then:
        parser.parse(string) == "Woah!"        
    }

    def "other kind of nesting"() {
        when:
        Parser parser = new Parser()
        parser.add("foo", '${bar}')
        parser.add("bar", 'test')
        parser.add("test", 'success!')
        def string = '${${foo}}'

        then:
        parser.parse(string) == "success!"
    }

    def "missing variable produces error message"() {
        when:
        Parser parser = new Parser()
        parser.add("baz", "lucky")
        def string = '${foo} ${baz}!'

        then:
        parser.parse(string) == "[Error: no mapping found for 'foo'] lucky!"
    }

    def "escaped variables are not resolved"() {
        when:
        Parser parser = new Parser()
        parser.add("foo", "treat")
        parser.add("baz", "lucky")
        def string = '\\${foo} \\${baz}!'

        then:
        parser.parse(string) == '${foo} ${baz}!'
    }

    /** test default values **/
    def "default value returned if key not added to parser"() {
        when:
        Parser parser = new Parser()
        parser.add("baz", "lucky")
        def string = '${bar:Earth} ${baz}!'

        then:
        parser.parse(string) == "Earth lucky!"
    }

    def "default value NOT returned if key present"() {
        when:
        Parser parser = new Parser()
        parser.add("baz", "lucky")
        parser.add("bar", "Mars")
        def string = '${bar:Earth} ${baz}!'

        then:
        parser.parse(string) == "Mars lucky!"
    }

    /** test optional variables **/
    def "optional variable that is present is parsed correctly"() {
        when:
        Parser parser = new Parser()
        parser.add("bar", "Mars")
        parser.add("baz", "Attacks")
        def string = '${bar} ${?baz}!'

        then:
        parser.parse(string) == "Mars Attacks!"
    }

    def "nothing is inserted for missing optional variable"() {
        when:
        Parser parser = new Parser()
        parser.add("bar", "Mars")
        def string = '${bar} ${?baz}!'

        then:
        parser.parse(string) == "Mars !"
    }

    /** test format specifiers **/
    def "format specifier is able to create a substring from the value"() {
        when:
        Parser parser = new Parser()
        parser.add("bar", "Noodles")
        parser.add("baz", "more")
        def string = '${bar;0,2} ${baz}!'

        then:
        parser.parse(string) == "No more!"
    }
}
